libcdict installation instructions

Copyright 2023 Robert Izzard

https://gitlab.com/rob.izzard/libcdict

------------------------------------------------------------

Requirements
------------

You will need a C compiler, e.g. gcc or clang. Your system should
provide packages for one of these.

You will need meson and ninja. Please install the latest versions
using pip3, e.g.

pip3 install meson
pip3 install ninja

and if you have these already installed

pip3 install --upgrade meson
pip3 install --upgrade ninja

------------------------------------------------------------

While I have made every effort to make sure the libcdict code
works on all platforms, it has only been serious tested on a 
modern Intel i7 processor on Linux, specifically the Ubuntu
20 and 22 distributions.

------------------------------------------------------------

To install a release version in your HOME directory, with the cdict
shared library (libcdict.so) in $HOME/lib, include files in
$HOME/include/cdict and the cdict-config executable in $HOME/bin, run:

~~~bash

meson setup --prefix=$HOME --libdir=lib --buildtype=release builddir
cd builddir
ninja install

~~~

To install a debug version in your HOME directory, run:

~~~bash

meson setup --prefix=$HOME --libdir=lib --buildtype=debug builddir
cd builddir
ninja install

~~~

To install a generic version (e.g. can run on any x86 CPU type)
in your HOME directory, run:

~~~bash

meson setup --prefix=$HOME --libdir=lib --buildtype=release -Dgeneric=true builddir
cd builddir
ninja install

~~~

To build without debugging symbols a version that is as fast as
possible on your PC, and that installs in your HOME directory, run:

~~~bash

meson setup --prefix=$HOME --libdir=lib --buildtype=release -Ddebugging_symbols=false builddir
cd builddir
ninja install

~~~

To build a version suitable for use with valgrind (this disables some gcc features that are incompatible with the current latest valgrind).

~~~bash

meson setup --prefix=$HOME --libdir=lib --buildtype=debug -Dvalgrind=TRUE builddir
cd builddir
ninja install

~~~

Further meson instructions can be found at https://mesonbuild.com/