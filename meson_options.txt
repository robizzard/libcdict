# Meson options for libcdict

option('accurate',
       type : 'boolean',
       value: false,
       description: 'Accurate mathematics mode: compiler options are chosen to keep calculations as accurate as possible.'
      )
option('generic',
       type : 'boolean',
       value: false,
       description: 'Turns off CPU-specific optimization so that the executable can be used on different platforms. Useful for condor/slurm clusters.'
      )
option('libname',
       type : 'string',
       value: 'cdict',
       description: 'The libcdict shared-library name, usually "cdict" to give a file libcdict.so (on Linux).'
      )
option('debugging_symbols',
       type: 'boolean',
       value: true,
       description: 'Turn debugging symbols on or off (on by default).'
      )
option('valgrind',
       type: 'boolean',
       value: false,
       description: 'Turns off CPU-specific instruction sets that are not compatible with Valgrind.'
      )
option('32bit',
       type : 'boolean',
       value: false,
       description: 'Force a 32-bit build (e.g. for Raspberry Pi).') 
