#!/bin/bash
cd ${MESON_SOURCE_ROOT}

# wrapper to output the git revision
# (outputs nothing on failure)
d=$(date +%Y%m%d 2>/dev/null)
c=$(git rev-list --full-history --all --abbrev-commit 2>/dev/null | /usr/bin/wc -l | sed -e 's/^ *//')
h=$(git rev-list --full-history --all --abbrev-commit 2>/dev/null | head -1)

if [[  ! -z "$d" && ! -z "$c" && ! -z "$h" ]]; then
    echo "${c}:${d}:${h}"
fi
