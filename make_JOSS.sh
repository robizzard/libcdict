#!/bin/bash

# script to build the JOSS paper from
# the source files in the libcdict repostiory

# make the pdf and preprint (LaTeX)
docker \
    run \
    --rm \
    --volume $PWD:/data \
    --user $(id -u):$(id -g) \
    --env JOURNAL=joss \
    openjournals/inara \
    -p \
    -o pdf,preprint \
    paper.md


# alter the LaTeX and remake it
mkdir -p arxiv
cp paper.preprint.tex arxiv/libcdict.tex
cd arxiv
sed -i s/\\usepackage\{textcomp\}/\usepackage\{textcomp\}\\n\\\\renewcommand\{\\\\familydefault\}\{\\\\sfdefault\}/ libcdict.tex
pdflatex libcdict.tex
pdflatex libcdict.tex
cd ..

echo "Main paper is at paper.pdf"
echo "Arxiv pdf is at arxiv/libcdict.pdf"
