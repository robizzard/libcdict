#include "cdict.h"

/*
 * Get metadata from an entry
 */

__CDict_Nonnull_some_arguments(1)
CDict_API_function
void * cdict_get_metadata(const struct cdict_entry_t * const entry)
{
    return entry->metadata->data;
}
