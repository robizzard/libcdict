#define _GNU_SOURCE
#include <stdio.h>
#include "cdict.h"


/*
 * Wrapper for cdict_fast_double_parser to mimic the strtod
 * function of the standard C library.
 */
double cdict_fast_strtod(const char * nptr,
                          char ** const endptr)
{
    double x;
    char * const y = cdict_fast_double_parser(nptr,&x);
    if(endptr) *endptr = y;
    return x;
}
