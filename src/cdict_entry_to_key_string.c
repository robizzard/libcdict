#define _GNU_SOURCE
#include <stdio.h>
#include "cdict.h"

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_entry_to_key_string(struct cdict_t * const cdict,
                              struct cdict_entry_t * const entry,
                              const char * const format)
{
    /*
     * Wrapper to be able to use an entry to set a key string
     */
    return cdict_set_key_string(cdict,
                                &entry->key,
                                format);
}
