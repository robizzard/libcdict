#include "cdict.h"


/*
 * Convenience function to free the metadata
 */
CDict_API_function
void cdict_free_metadata(struct cdict_t * const cdict,
                         struct cdict_entry_t * entry)
{
    if(entry && entry->metadata)
    {
        CDict_Safe_free(cdict,
                        entry->metadata);
    }
}
