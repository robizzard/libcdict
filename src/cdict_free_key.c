#include "cdict.h"

void cdict_free_key(struct cdict_t * const cdict,
                    struct cdict_key_t ** const keyp,
                    const Boolean free_key)
{
    /*
     * Free the contents of a key and NULL it.
     */
    if(keyp!=NULL)
    {
        struct cdict_key_t * const key = *keyp;
        if(key!=NULL)
        {
            /* free key string */
            CDict_Safe_free(cdict,key->string);

            /* free mapped string */
            if(key->mapped == TRUE &&
               key->map_type == CDICT_DATA_TYPE_STRING &&
               key->map_data.string_data != NULL)
            {
                CDict_Safe_free(cdict,
                                key->map_data.string_data);
            }
        }
        if(free_key == TRUE)
        {
            /* free the actual key struct too */
            CDict_Safe_free(cdict,*keyp);
        }
    }
}
