#define _GNU_SOURCE
typedef int make_iso_compilers_happy;
#include "cdict.h"
#undef exit
#include "cdict_tests.h"
/*
 * Some tests of the libcdict library, called by
 * "cdict-config tests" on the command line.
 */

/*
 * Cdict_new should be redefined to always
 * set the error handler.
 */
#ifdef __CDICT_HAVE__VA_OPT__
#define CDict_New(DICT,...)                     \
    CDict_new(DICT __VA_OPT__(,) __VA_ARGS__);  \
    DICT->error_handler = _error_handler;
#else
#define CDict_New(DICT,...)                     \
    CDict_new(DICT , ##__VA_ARGS__);            \
    DICT->error_handler = _error_handler;
#endif

struct cdict_entry_list_t
{
    struct cdict_entry_t ** entries;
    size_t len;
};

CDict_API_function
void cdict_tests(void)
{
    /*
     * Run each test group in turn through the _test macro.
     * This exits on error.
     */

//    _test(JSON_load,"JSON load");exit(0);
    const Boolean valgrind __CDict_maybe_unused = test_for_valgrind();
    printf("libcdict tests : version %s, compiled by %s (\"%s\") [Meson project %s version %s]\n",
           CDICT_VERSION,
           COMPILER_ID,
           CC,
           __CDict_Stringify_macro(MESON_PROJECT_NAME),
           __CDict_Stringify_macro(MESON_PROJECT_VERSION));
#ifdef __CDICT_VARIABLES_TESTS__
    _test(variables,"Variables");
#else
    printf("Warning: variables test disabled (probably because you're not building with gcc)\n");
#endif//__CDICT_VARIABLES_TESTS__


    _test(data_saving_and_loading,"Data Saving and Loading");
    _test(numerics,"Numerics");
    _test(memory,"Memory",valgrind);
    _test(copying,"Copying");
    _test(nest,"Nest");
    _test(grep,"Grep");
    _test(sorting,"Sorting");
    _test(API_functions,"API functions");
    _test(API_macros,"API macros");
    _test(CDICT_NARGS_macros,"CDICT_NARG macros");
    _test(iter,"Iter functionality");
    _test(cdict_arrays,"Arrays of cdicts");
    printf("libcdict : all tests passed\n");
}

static int _error_handler(void * p,
                          const int error_number,
                          const char * const format,
                          va_list args)
{
    /*
     * Catch errors and exit on error.
     */
    const void * const q __CDict_maybe_unused = p;
    printf("libcdict tests: error %d detected\n",error_number);
    vprintf(format,args);
    printf("Tests failed: exiting cdict tests.\n");
    return 1;
}

static int _dummy_error_handler(void * p,
                                const int error_number __CDict_maybe_unused,
                                const char * const format __CDict_maybe_unused,
                                va_list args __CDict_maybe_unused)
{
    /*
     * Dummy error handler. Return 0 to prevent exit.
     */
    struct cdict_t * cdict = (struct cdict_t *)p;
    CDict_set(cdict,"dummy error handler",(int)666);
    return 0;
}

#ifdef __CDICT_VARIABLES_TESTS__
static int _cdict_test_variables(void)
{
    /*
     * Make a cdict containing every type of variable
     * as a key and value, and compare the JSON output
     * to a reference set of results.
     */
    int result = CDICT_TEST_PASSED;
    unsigned int i = 0;

#undef X
#define X(TYPE,                                                         \
          MEMBER,                                                       \
          CTYPE,                                                        \
          DEFCTYPE,                                                     \
          CARRAY,                                                       \
          DESCRIPTOR,                                                   \
          FORMAT,                                                       \
          DEREF_FORMAT,                                                 \
          DEREF_CTYPE,                                                  \
          GROUP,                                                        \
          ARRAY_TO_SCALAR_MAPPER,                                       \
          SCALAR_TO_ARRAY_MAPPER,                                       \
          NUMERIC,                                                      \
          DEMO)                                                         \
    {                                                                   \
        if(result == CDICT_TEST_PASSED &&                               \
           CDICT_DATA_TYPE_##TYPE != CDICT_DATA_TYPE_UNKNOWN &&         \
           CDICT_DATA_TYPE_##TYPE != CDICT_DATA_TYPE_CDICT_ARRAY)       \
        {                                                               \
                                                                        \
            if(generate_results)                                        \
            {                                                           \
                printf("//Test variable %s (CTYPE %s)\n",               \
                       DESCRIPTOR,                                      \
                       #DEFCTYPE #CARRAY);                              \
                fflush(stdout);                                         \
            }                                                           \
                                                                        \
            /* make new cdict */                                        \
            CDict_New(cdict);                                           \
                                                                        \
            /* make demo data */                                        \
            DEFCTYPE _test_data CARRAY __CDict_maybe_unused = DEMO;     \
            char * s;                                                   \
            if(asprintf(&s,"test of type %s",DESCRIPTOR)==0)            \
            {                                                           \
                _test_failed("asprintf malloc failed");                 \
            }                                                           \
                                                                        \
            /* nest data in the cdict */                                \
            struct cdict_entry_t * const e =                            \
                CDict_nest_set(cdict,                                   \
                               (char*)s,                                \
                               _test_data,                              \
                               "test data");                            \
                                                                        \
            if(CDICT_DATA_TYPE_##TYPE == CDICT_DATA_TYPE_CHAR_POINTER)  \
            {                                                           \
                /* char *, not string!, type: special case */           \
                e->value.type = CDICT_DATA_TYPE_CHAR_POINTER;           \
                e->value.format = "%p";                                 \
            }                                                           \
                                                                        \
            /* convert to JSON and display */                           \
            char * json_buffer = NULL;                                  \
            size_t json_size = 0;                                       \
            CDict_to_JSON(cdict,                                        \
                          json_buffer,                                  \
                          json_size,                                    \
                          CDICT_JSON_WHITESPACE);                       \
            if(generate_results)                                        \
            {                                                           \
                printf("R\"\"\"\"(%s)\"\"\"\",\n",json_buffer);         \
            }                                                           \
            else if(strcmp(json_buffer,                                 \
                           __cdict_test_variables_expected[i])!=0)      \
            {                                                           \
                printf("Test variable %s (CTYPE %s)\n",                 \
                       DESCRIPTOR,                                      \
                       #DEFCTYPE #CARRAY);                              \
                printf("test %u failed\n",i);                           \
                printf("generated JSON : \n%s\n",                       \
                       json_buffer);                                    \
                printf("expected  JSON : \n%s\n",                       \
                       __cdict_test_variables_expected[i]);             \
                result = CDICT_TEST_FAILED;                             \
                fflush(stdout);                                         \
            }                                                           \
                                                                        \
            /* free memory */                                           \
            CDict_Safe_free(cdict,json_buffer);                         \
            CDict_Safe_free(cdict,s);                                   \
            CDict_free(cdict);                                          \
            i++;                                                        \
        }                                                               \
    }                                                                   \

    __CDICT_DATA_TYPES__;
#undef X
    if(result == CDICT_TEST_FAILED)
    {
        _test_failed("variable test %u failed",i);
    }
    return result;
}

#endif // __CDICT_VARIABLES_TESTS__

static int _cdict_test_numerics(void)
{
    /*
     * Test numeric types.
     */
    int result = CDICT_TEST_PASSED;

    /* make cdic */
    CDict_New(cdict);

    /* set numerics to have rather poor accuracy for the test */
    CDict_set_numerics(0.1,0.1,3,3);

    if(vb)
    {
        printf("single-precision cdict keys have %zu bytes of precision\n",
               cdict_hashv_float_nbytes);
        printf("double-precision cdict keys have %zu bytes of precision\n",
               cdict_hashv_double_nbytes);
        printf("long-double-precision cdict keys have %zu bytes of precision\n",
               cdict_hashv_long_double_nbytes);
    }

    if(cdict_hashv_float_nbytes != 3 ||
       cdict_hashv_double_nbytes != 3 ||
       cdict_hashv_long_double_nbytes != 3)
    {
        _test_failed("Failed to set libcdict numerics correctly");
    }

    /*
     * Test accuracy of double types
     * by comparing float, double and long double
     */
#define __DOUBLE_TYPES                              \
    X(float,float,M_PI,FLT_MIN)                     \
        X(double,double,M_PI,DBL_MIN)               \
        X(long double,long_double,M_PIl,LDBL_MIN)

#undef X
#define X(CTYPE,TRUNCFUNC,PICONST,MINVAL)                               \
    /* make (1+eps) * pi */                                             \
    {                                                                   \
        CTYPE x0 = PICONST;                                             \
        struct cdict_key_t key0;                                        \
        key0.type = __CDict_valuetype_num(x0);                          \
        key0.format = NULL;                                             \
        key0.key = __CDict_key_union_setter(x0)(x0);                    \
        unsigned cdictv0;                                               \
        cdict_truncate_##TRUNCFUNC(x0,cdict_hashv_##TRUNCFUNC##_nbytes); \
        CDICT_UTHASH_JEN(cdict,                                         \
                         &key0,                                         \
                         sizeof(CTYPE),                                 \
                         cdictv0,                                       \
                         __COUNTER__);                                  \
        if(vb)                                                          \
        {                                                               \
            printf("type %s CDICTV0 %12u\n",                            \
                   #CTYPE,                                              \
                   cdictv0);                                            \
        }                                                               \
                                                                        \
        CTYPE eps = 1.0;                                                \
        while(eps > 1e-20)/* MINVAL )*/                                 \
        {                                                               \
            const CTYPE x = PICONST * (1.0 + eps);                      \
            struct cdict_key_t k;                                       \
            cdict_truncate_##TRUNCFUNC(x,cdict_hashv_double_nbytes);    \
                                                                        \
            k.type = key0.type;                                         \
            k.key = __CDict_key_union_setter(x)(x);                     \
            k.format = NULL;                                            \
                                                                        \
            unsigned cdictv;                                            \
            CDICT_UTHASH_JEN(cdict,                                     \
                             &k,                                        \
                             sizeof(CTYPE),                             \
                             cdictv,                                    \
                             __COUNTER__);                              \
                                                                        \
            if(vb)                                                      \
            {                                                           \
                printf("CDICTV %s of (1 + %16Lg) * pi = %12u vs %12u (key %s, number %s)\n", \
                       #CTYPE,                                          \
                       (long double)eps,                                \
                       cdictv,                                          \
                       cdictv0,                                         \
                       cdictv==cdictv0 ? "same" : "differs",            \
                       x==x0 ? "same" : "differs"                       \
                    );                                                  \
            }                                                           \
            if(cdictv == cdictv0)                                       \
            {                                                           \
                /* hit accuracy */                                      \
                printf("%20s accuracy : %Lg\n",                         \
                       #CTYPE,                                          \
                       (long double)eps);                               \
                eps = 0.0;                                              \
            }                                                           \
            eps*=0.1;                                                   \
        }                                                               \
    }
    __DOUBLE_TYPES;
    if(vb)
    {
        CDict_stats(cdict);
    }
    CDict_free(cdict);

    return result;
}

static int _cdict_test_data_saving_and_loading(void)
{
    /*
     * Test saving and loading data:
     * we use tmpfile to give us a location for the data.
     */
    int result = CDICT_TEST_PASSED;

    /*
     * Make a cdict
     */
    CDict_New(cdict);

    CDict_nest(cdict,
               1,"Bergkamp Arsenal",
               "Denis","10");
    CDict_nest(cdict,
               2.0,"nested data",
               "hello");
    CDict_nest(cdict,
               TRUE,"Bergkamp Ajax",
               "Denis",10);
    CDict_nest_set(cdict,
                   "Robert Pires",7,
                   "Best player ever");
    CDict_nest_set(cdict,
                   "array demo",((int[]){1,2,3}),
                   "demo");
    /*
     * Convert to JSON
     */
    char * json_buffer = NULL;
    size_t json_size = 0;
    CDict_to_JSON(cdict,
                  json_buffer,
                  json_size,
                  CDICT_JSON_WHITESPACE);

    if(vb)
    {
        printf("JSON buffer at %p, size %zu\n",(void*)json_buffer,json_size);
        printf("%s\n",json_buffer);
    }

    /*
     * Save to a temporary file and check the results
     */
    char loc[] = "/tmp/XXXXXX";
    char * const tmpdir = mkdtemp(loc);

    if(tmpdir==NULL)
    {
        _test_failed("Failed to make temporary file name.");
    }
    else
    {
        char * file;
        if(asprintf(&file,
                    "%s/%s",
                    tmpdir,
                    "out.json")==0)
        {
            _test_failed("Failed to allocate memory with asprintf");
        }
        if(vb)
        {
            printf("Saving temporary JSON file to %s\n",file);
        }
        if(file)
        {
            /*
             * Write the data
             */
            CDict_to_JSON_file(cdict,
                               file,
                               CDICT_JSON_WHITESPACE);

            /*
             * Free cdict
             */
            CDict_free(cdict);
            cdict = NULL;

            /*
             * Check file and buffer match
             */
            char * saved_buffer = _slurp(file);
            free(file);

            if(strcmp(saved_buffer,json_buffer)!=0)
            {
                free(saved_buffer);
                _test_failed("Saved JSON buffer did not match");
            }
            else
            {
                free(saved_buffer);
            }
        }
        else
        {
            _test_failed("Could not make string with filename\n");
        }
    }

    CDict_Safe_free(cdict,json_buffer);
    return result;
}

static int _cdict_test_sorting(void)
{
    int result = CDICT_TEST_PASSED;

    /* set numerics to have rather poor accuracy for the test */
    CDict_set_numerics(10.0*DBL_EPSILON,10.0*DBL_EPSILON,5,5);

    CDict_New(h1);

    int i = 20;
    while(i>0)
    {
        const double r1 = 1.0 + pow(10.0,-(double)i);
        const double r2 = (double)(99.0*(double)rand()/(double)RAND_MAX + 1.0);
        if(vb)
        {
            printf("set %30.15e (1-%g) %30.15e\n",r1,1.0-r1,r2);
        }
        CDict_nest(h1,
                   i,r1,
                   "test data");
        i--;
    }

    char * json_buffer = NULL;
    size_t json_size = 0;
    CDict_to_JSON(h1,
                  json_buffer,
                  json_size,
                  CDICT_JSON_WHITESPACE);

    struct cdict_t * h2  = *(struct cdict_t**)CDict_nest_get_data_pointer(h1,"test data");
    double prev = -1.0;
    CDict_sorted_loop(h2,entry)
    {
        char * valuestring;
        const int ret = CDict_entry_to_key_and_value_strings(h2,
                                                             entry,
                                                             valuestring);
        if(ret==-2)
        {
            _test_failed("Entry to key and value strings failed");
        }

        char * endptr;
        const double v = CDict_strtod(valuestring,&endptr);
        if(vb)
        {
            printf("v=%g from %s prev=%g\n",v,valuestring,prev);
        }
        if(prev>-0.5)
        {
            if(v>prev)
            {
                _test_failed("sorting failed : value %g > previous value %g\n",
                             v,
                             prev);
            }
        }
        prev = v;

        CDict_Safe_free(h1,valuestring);
    }

    CDict_Safe_free(h1,json_buffer);
    CDict_free(h1);
    return result;
}

static int _cdict_test_copying(void)
{
    int result = CDICT_TEST_PASSED;

    /*
     * Make a cdict containing some obscure data,
     * copy it, and check that the JSON made is identical
     * from the copy and the original.
     */

    CDict_New(cdict);
    CDict_nest(cdict,
               "demo nested int",1,
               "top level","nested location");
    CDict_nest(cdict,
               "top level string","!!!",
               "top level");
    CDict_nest(cdict,
               "demo nested float",(float)1.234,
               "top level","nested location");
    CDict_nest(cdict,
               "demo nested double",(double)5.6789,
               "top level","nested location");
    CDict_nest(cdict,
               "demo nested Boolean",(Boolean)TRUE,
               "top level","nested location");
    CDict_nest(cdict,
               "demo array of integers",((int []){1,2,3,4}),
               "top level","nested location");
    CDict_nest(cdict,
               "demo array of long doubles",((long double []){10.0,-20.0,30.0,-40.0}),
               "top level");
    CDict_nest(cdict,
               "demo array of strings",((char *[]){"hello", "world"}),
               "top level");

    char * json_buffer = NULL;
    size_t json_size = 0;
    CDict_to_JSON(cdict,
                  json_buffer,
                  json_size,
                  CDICT_JSON_WHITESPACE);

    CDict_New(cdict_copied);
    cdict_copy(cdict,cdict_copied);

    char * json_buffer_copied = NULL;
    size_t json_size_copied = 0;
    CDict_to_JSON(cdict_copied,
                  json_buffer_copied,
                  json_size_copied,
                  CDICT_JSON_WHITESPACE);
    if(vb)
    {
        printf("ORIGINAL\n");
        printf("%zu\n",json_size);
        printf("%s\n",json_buffer);
        printf("\n\n\nCOPY\n");
        printf("%zu\n",json_size_copied);
        printf("%s\n",json_buffer_copied);
    }

    if(json_size != json_size_copied ||
       strcmp(json_buffer,json_buffer_copied)!=0)
    {
        _test_failed("JSON buffer does not equal copied JSON buffer");
    }

    CDict_Safe_free(cdict,json_buffer);
    CDict_Safe_free(cdict_copied,json_buffer_copied);
    CDict_free(cdict);
    CDict_free(cdict_copied);
    return result;
}


/*
 * Numbers greater than GREP_LIMIT should
 * match in the grep
 */
#define GREP_MIN 0
#define GREP_LIMIT 50
#define GREP_MAX 100
#define GREP_NUM 20

static Boolean grep_func(struct cdict_entry_t * entry)
{
    /*
     * Return TRUE if the key of this entry is > 50,
     * otherwise FALSE.
     */
    return entry->key.key.int_data > GREP_LIMIT ? TRUE : FALSE;
}

static int _cdict_test_grep(void)
{
    /*
     * Make a cdict containing random numbers,
     * and use libcdict's grep functionality to detect
     * which cdict entries satisfy TRUE from the grep_func
     * (see above).
     */
    CDict_New(cdict);
    const int upper = GREP_MAX;
    const int lower = GREP_MIN;
    int n_expected = 0;
    int used[GREP_MAX+1] = {0};
    int i = 0;
    time_t t;
    srand((unsigned) time(&t));
    while(++i < GREP_NUM)
    {
        const int r = 1 + rand() % (upper-lower) + lower;
        if(r > GREP_LIMIT)
        {
            if(used[r]==0)n_expected++;
            used[r]++;
        }
        CDict_nest(cdict,
                   (int)r,1,
                   "random number list");
    }
    if(vb)
    {
        CDict_print_JSON(cdict,CDICT_JSON_SORT,stdout);
    }

    /*
     * use CDict_grep to find matching entries
     */
    cdict_size_t nmatches;
    struct cdict_entry_t ** match_entries =
        CDict_grep(CDict_nest_get_entry(cdict,"random number list")->value.value.cdict_pointer_data,
                   grep_func,
                   &nmatches);
    if(vb)
    {
        printf("grep: matched %lld, expected %d\n",
               nmatches,
               n_expected);
    }
    const int result = ((int)nmatches) != n_expected ? CDICT_TEST_FAILED : CDICT_TEST_PASSED;

    CDict_Safe_free(cdict,match_entries);
    CDict_free(cdict);

    return result;
}

static int _cdict_test_memory(const Boolean valgrind)
{
    int result = CDICT_TEST_PASSED;
    /* number of memory tests is fewer if we're running valgrind */
    const int n = valgrind == TRUE ? 1000 : 100000;
    struct cdict_t * tofree[CDICT_DATA_NUMBER_OF_TYPES] = {0};
    int ntest = 0;
#undef X
#define X(TYPE,                                                 \
          MEMBER,                                               \
          CTYPE,                                                \
          DEFCTYPE,                                             \
          CARRAY,                                               \
          DESCRIPTOR,                                           \
          FORMAT,                                               \
          DEREF_FORMAT,                                         \
          DEREF_CTYPE,                                          \
          GROUP,                                                \
          ARRAY_TO_SCALAR_MAPPER,                               \
          SCALAR_TO_ARRAY_MAPPER,                               \
          NUMERIC,                                              \
          DEMO)                                                 \
    {                                                           \
        if(CDICT_DATA_TYPE_##TYPE != CDICT_DATA_TYPE_UNKNOWN)   \
        {                                                       \
            int mem_start,mem_end;                              \
            getMemory(&mem_start,NULL,NULL,NULL);               \
                                                                \
            CDict_New(cdict);                                   \
            tofree[ntest++] = cdict;                            \
            for(int i=0;i<n;i++)                                \
            {                                                   \
                CDict_nest(cdict,                               \
                           (int)i,1,                            \
                           "random number list");               \
            }                                                   \
                                                                \
            getMemory(&mem_end,NULL,NULL,NULL);                 \
                                                                \
            printf("%30s uses %10.2f MB = %10g kB each\n",      \
                   DESCRIPTOR,                                  \
                   -(mem_start - mem_end)/1000.0,               \
                   -(mem_start - mem_end)/(double)n             \
                );                                              \
        }                                                       \
    }

    /* run the tests */
    __CDICT_DATA_TYPES__;

    /* free memory */
    for(int i=0;i<CDICT_DATA_NUMBER_OF_TYPES;i++)
    {
        CDict_free(tofree[i]);
    }


#define __CDICT_STRUCTS                         \
    X(key)                                      \
        X(entry)                                \
        X(value)                                \
        X(metadata)                             \
        X(pre_output)                           \
        X(pointer_list_item)                    \
        X(stats)                                \
        X(tofree)                               \
        X(iterator)                             \
        X(entry_stack)                          \
        X(entry_stack_item)

    /* struct sizes */
    printf("Struct sizes:\n  %25s %zu\n",
           "cdict_t",
           sizeof(struct cdict_t));
#undef X
#define X(S)                                    \
    printf("  %25s %zu\n",                      \
           "cdict_" #S "_t",                    \
           sizeof(struct cdict_## S ##_t)       \
        );
    __CDICT_STRUCTS;
#undef X

    return result;
}

static void getMemory(int * const currRealMem,
                      int * const peakRealMem,
                      int * const currVirtMem,
                      int * const peakVirtMem)
{
    /*
     * Measures the current (and peak) resident and virtual memories
     * usage of your linux C process, in kB
     *
     * Based on:
     * https://stackoverflow.com/questions/1558402/memory-usage-of-current-process-in-c
     *
     * Each integer pointer is set to zero such that, on failure, they are
     * all zero.
     */
    static char filename[] = "/proc/self/status";
    if(currRealMem) *currRealMem = 0;
    if(peakRealMem) *peakRealMem = 0;
    if(currVirtMem) *currVirtMem = 0;
    if(peakVirtMem) *peakVirtMem = 0;
    if(access(filename,F_OK) == 0)
    {
        // linux file contains this-process info
        FILE * file = fopen(filename, "r");
        char buffer[1024] = "";

        // read the entire file
        while (fscanf(file, " %1023s", buffer) == 1)
        {
            if(currRealMem && strcmp(buffer, "VmRSS:") == 0)
            {
                if(fscanf(file, " %d", currRealMem)==0)
                {
                    /* ... */
                }
            }
            if(peakRealMem && strcmp(buffer, "VmHWM:") == 0)
            {
                if(fscanf(file, " %d", peakRealMem))
                {
                    /* ... */
                }
            }
            if(currVirtMem && strcmp(buffer, "VmSize:") == 0)
            {
                if(fscanf(file, " %d", currVirtMem))
                {
                    /* ... */
                }
            }
            if(peakVirtMem && strcmp(buffer, "VmPeak:") == 0)
            {
                if(fscanf(file, " %d", peakVirtMem))
                {
                    /* ... */
                }
            }
        }
        fclose(file);
    }
}

static int _cdict_test_API_functions(void)
{
    /*
     * Test each API function in turn, using _test_failed()
     * in case something goes wrong.
     */

    {
        /*
         * cdict_new()
         */
        struct cdict_t * cdict = cdict_new();
        if(cdict == NULL)
        {
            _test_failed("Failed to allocate new cdict.");
        }

        /*
         * Test cdict's pointers are allocated
         */
        if(cdict->ancestor != NULL ||
           cdict->parent != NULL ||
           cdict->use_cache != TRUE ||
           cdict->force_maps != TRUE)
        {
            _test_failed("New cdict isn't set up properly.");
        }

        /*
         * cdict_free()
         */
        cdict_free(&cdict,TRUE,NULL);

        /*
         * There's no good, portable way to
         * detect whether a free() call actually
         * works... cf.
         * https://stackoverflow.com/questions/40999454/checking-if-free-worked
         */
    }

    /*
     * cdict_set()
     */
    {
        struct cdict_t * cdict = cdict_new();
        if(cdict == NULL)
        {
            _test_failed("Failed to allocate new cdict");
        }

        {
            union cdict_key_union keydata;
            union cdict_value_union valuedata;
            keydata.double_data = 1.0;
            valuedata.double_array_data = (double[]){1.0,2.0,3.0};
            struct cdict_entry_t * const entry =
                cdict_set(cdict,
                          keydata,
                          CDICT_DATA_TYPE_DOUBLE,
                          valuedata,
                          3,
                          CDICT_DATA_TYPE_DOUBLE_ARRAY,
                          NULL,
                          NULL);
            if(entry == NULL)
            {
                _test_failed("Failed to set cdict entry");
            }
        }

        /*
         * cdict_set_with_formats()
         */
        {
            union cdict_key_union keydata;
            union cdict_value_union valuedata;
            keydata.double_data = 1.0;
            valuedata.double_array_data = (double[]){1.0,2.0,3.0};
            struct cdict_entry_t * const entry =
                cdict_set_with_formats(cdict,
                                       keydata,
                                       CDICT_DATA_TYPE_DOUBLE,
                                       "double key %12.10e",
                                       valuedata,
                                       3,
                                       CDICT_DATA_TYPE_DOUBLE_ARRAY,
                                       "double in array %5.2f",
                                       NULL,
                                       NULL);
            if(entry == NULL)
            {
                _test_failed("Failed to set cdict entry.");
            }
            if(entry->value.count != 3)
            {
                _test_failed("Set the wrong number of array elements.");
            }

            /*
             * test using cdict_value_to_value_string()
             */
            char * string;
            cdict_value_to_value_string(cdict,
                                        &entry->value,
                                        &string,
                                        entry->value.format,
                                        0);
            char * const should_be = "double in array  1.00";
            if(strcmp(string,should_be)!=0)
            {
                _test_failed("Failed to set value string correctly\nSTRING : \"%s\"\nSHOULD BE   : \"%s\"\n",
                             string,
                             should_be);
            }
            free(string);

            /*
             * cdict_append
             */
            union cdict_value_union value_to_append;
            value_to_append.double_array_data = (double[]){4.0,5.0,6.0,7.0};
            cdict_append(cdict,
                         keydata,
                         CDICT_DATA_TYPE_DOUBLE,
                         value_to_append,
                         4,
                         CDICT_DATA_TYPE_DOUBLE_ARRAY,
                         NULL,
                         NULL);

            /*
             * cdict_contains()
             *
             * Get the resulting entry and hence the array
             */
            struct cdict_entry_t * const entry2 =
                cdict_contains(cdict,
                               keydata,
                               CDICT_DATA_TYPE_DOUBLE);

            if(entry2 == NULL)
            {
                _test_failed("Appended entry was not found.");
            }

            /*
             * Test number of items stored
             */
            if(entry2->value.count != 7)
            {
                _test_failed("Appended entry array length is %lld, should be 7, this is incorrect\n",
                             entry2->value.count);
            }

            /*
             * Note: here we compare doubles directly. This comparison
             * uses != because they should be exactly equal.
             */
            for(int i=0;i<7;i++)
            {
                if(entry2->value.value.double_array_data[i] != (double)(i+1))
                {
                    _test_failed("entry double array value[%d] = %g is incorrect, should be %g.",
                                 i,
                                 entry2->value.value.double_array_data[i],
                                 (double)(i+1)
                        );
                }
            }

            /*
             * cdict_append_given_entry
             */
            union cdict_value_union value_to_append2;
            value_to_append2.double_array_data = (double[]){8.0,9.0,10.0};
            cdict_append_given_entry(cdict,
                                     entry2,
                                     CDICT_DATA_TYPE_DOUBLE,
                                     value_to_append2,
                                     3,
                                     CDICT_DATA_TYPE_DOUBLE_ARRAY,
                                     NULL);

            if(entry2->value.count != 10)
            {
                _test_failed("Appended entry array length is %lld, should be 10, this is incorrect.",
                             entry2->value.count);
            }

            /*
             * cdict_set_metadata
             */
            void * metadata;
            char * metadata_string;
            if(asprintf(&metadata_string,
                        "Here is some metadata")==0)
            {
                _test_failed("Failed to allocate memory with asprintf");
            }
            metadata = (void*) metadata_string;
            cdict_set_metadata(cdict,
                               entry2,
                               metadata,
                               _metadata_freer);

            /*
             * cdict_get_metadata
             */
            char * const got_metadata_string = (char*)cdict_get_metadata(entry2);
            if(strcmp(got_metadata_string,
                      "Here is some metadata")!=0)
            {
                _test_failed("Metadata was stored incorrectly: did not return the string expected.");
            }

            /*
             * cdict_contains_cdicts
             */
            if(cdict_contains_cdicts(cdict)==TRUE)
            {
                _test_failed("we seem to think this cdict has cdict children, which it should not.");
            }


            /*
             * Set entry key and value to string
             */
            cdict_entry_to_key_string(cdict,
                                      entry2,
                                      NULL);
            if(entry2->key.string == NULL)
            {
                _test_failed("failed to convert entry2's key to string.");
            }
            char * valuestring;
            if(cdict_entry_to_value_string(cdict,
                                           entry2,
                                           &valuestring,
                                           NULL)<=0)
            {
                _test_failed("failed to convert entry2's value to string.");
            }
            if(strcmp(entry2->key.string,"double key 1.0000000000e+00")!=0)
            {
                _test_failed("key should be \"double key 1.0000000000e+00\" but is not, it is \"%s\".",entry2->key.string);
            }
            if(strcmp(valuestring,
                      "[\"double in array  1.00\",\"double in array  2.00\",\"double in array  3.00\",\"double in array  4.00\",\"double in array  5.00\",\"double in array  6.00\",\"double in array  7.00\",\"double in array  8.00\",\"double in array  9.00\",\"double in array 10.00\"]")!=0)
            {
                _test_failed("value string (array of doubles) was made incorrectly.");
            }
            CDict_Safe_free(cdict,entry2->key.string);
            CDict_Safe_free(cdict,valuestring);

            /*
             * Try the combined call to
             * cdict_entry_to_key_and_value_strings
             */
            char * valuestring2;
            if(cdict_entry_to_key_and_value_strings(cdict,
                                                    entry2,
                                                    &valuestring2)<=0)
            {
                _test_failed("failed to convert entry2's value to string.");
            }
            if(strcmp(entry2->key.string,"double key 1.0000000000e+00")!=0)
            {
                _test_failed("key should be \"double key 1.0000000000e+00\" but is not, it is \"%s\".",entry2->key.string);
            }
            if(strcmp(valuestring2,
                      "[\"double in array  1.00\",\"double in array  2.00\",\"double in array  3.00\",\"double in array  4.00\",\"double in array  5.00\",\"double in array  6.00\",\"double in array  7.00\",\"double in array  8.00\",\"double in array  9.00\",\"double in array 10.00\"]")!=0)
            {
                _test_failed("value string (array of doubles) was made incorrectly.");
            }
            CDict_Safe_free(cdict,entry2->key.string);
            CDict_Safe_free(cdict,valuestring2);

            /*
             * cdict_key_and_value_strings
             */
            char * valuestring3;
            if(cdict_key_and_value_strings(cdict,
                                           &entry2->key,
                                           &entry2->value,
                                           &valuestring3)<=0)
            {
                _test_failed("failed to convert entry2's value to string.");
            }
            if(strcmp(entry2->key.string,"double key 1.0000000000e+00")!=0)
            {
                _test_failed("key should be \"double key 1.0000000000e+00\" but is not, it is \"%s\".",entry2->key.string);
            }

            if(strcmp(valuestring3,
                      "[\"double in array  1.00\",\"double in array  2.00\",\"double in array  3.00\",\"double in array  4.00\",\"double in array  5.00\",\"double in array  6.00\",\"double in array  7.00\",\"double in array  8.00\",\"double in array  9.00\",\"double in array 10.00\"]")!=0)
            {
                _test_failed("value string (array of doubles) was made incorrectly.");
            }
            CDict_Safe_free(cdict,valuestring3);

            /*
             * cdict_entry_index_to_value_string
             * Get values at different array locations
             */
            for(cdict_size_t i=0;i<10;i++)
            {
                char * vstring = NULL;
                if(cdict_entry_index_to_value_string(cdict,
                                                     entry,
                                                     &vstring,
                                                     NULL,
                                                     i))
                {
                    char * should_be_string;
                    if(asprintf(&should_be_string,
                                "double in array %5.2f",
                                (double)(i+1)))
                    {
                        if(strcmp(should_be_string,
                                  vstring)!=0)
                        {
                            free(should_be_string);
                            _test_failed("Failed to match index entry %lld string with expected string.",
                                         i);
                        }
                        free(should_be_string);
                    }
                    else
                    {
                        _test_failed("Failed to asprintf string.");
                    }
                }
                CDict_Safe_free(cdict,vstring);
            }

            /*
             * cdict_get_union_data_pointer
             */
            void * p = cdict_get_union_data_pointer(entry2);
            if(p!=&entry2->value.value)
            {
                _test_failed("cdict_get_union_data_pointer returns a pointer that is not the same as &entry2->value.value ... these should be equal.");
            }
        }


        {
            /*
             * cdict_error
             */
            cdict->error_handler = _dummy_error_handler;
            cdict->error_data = (void*)cdict;


            if(CDict_contains(cdict,
                              "dummy error handler")!=NULL)
            {
                _test_failed("found \"dummy error handler\" key in cdict when it should not be there.");
            }

            cdict_error(cdict,666,"");

            if(CDict_contains(cdict,
                              "dummy error handler")==NULL)
            {
                _test_failed("failed to find \"dummy error handler\" key in cdict when it should be there.");
            }
            cdict->error_handler = _error_handler;
            cdict->error_data = NULL;

        }

        CDict_free(cdict);
    }

    return CDICT_TEST_PASSED;
}

static void _metadata_freer(struct cdict_t * const cdict __CDict_maybe_unused,
                            struct cdict_entry_t * entry __CDict_maybe_unused)
{
    /* free data in here */
    if(entry->metadata != NULL &&
       entry->metadata->data != NULL)
    {
        CDict_Safe_free(cdict,
                        entry->metadata->data);
    }
}

static int _cdict_test_nest(void)
{
    CDict_New(cdict);

    CDict_nest(cdict,
               "one",(double)1.0,
               "doubles");
    CDict_nest(cdict,
               "one",(int)1,
               "integers");
    CDict_nest(cdict,
               "one",(int)1,
               "integers",
               "positive");
    CDict_nest(cdict,
               "two",(int)2,
               "integers",
               "positive");
    CDict_nest(cdict,
               "minus one",(int)-1,
               "integers",
               "positive");
    char * json_buffer = NULL;
    size_t json_size = 0;
    CDict_to_JSON(cdict,
                  json_buffer,
                  json_size,
                  CDICT_JSON_NO_WHITESPACE);

    if(!strcmp("{\"doubles\":{\"one\":1},\"integers\":{\"one\":1,\"positive\":{\"one\":1,\"two\":2,\"minus one\":-1}}}\n",
               json_buffer))
    {
        _test_failed("Nested cdict JSON buffer is incorrect.");
    }
    CDict_Safe_free(cdict,json_buffer);

    CDict_free(cdict);

    return CDICT_TEST_PASSED;
}

static int _cdict_test_CDICT_NARGS_macros(void)
{

    if(CDICT_NARGS()!=0 ||
       CDICT_NARGS(x)!=1 ||
       CDICT_NARGS(x,y)!=2 ||
       CDICT_NARGS(x,y,z)!=3 ||
       CDICT_NARGS(w,x,y,z)!=4)
    {
        _test_failed("CDICT_NARGS macro failed\n");
    }

    return CDICT_TEST_PASSED;
}

#define _cdict_should_be(CDICT,JSON,...)                            \
    {                                                               \
        char * json_buffer = NULL;                                  \
        size_t json_size = 0;                                       \
        CDict_to_JSON((CDICT),                                      \
                      json_buffer,                                  \
                      json_size,                                    \
                      CDICT_JSON_NO_WHITESPACE);                    \
        if(strcmp((JSON),json_buffer)!=0)                           \
        {                                                           \
            printf("%s\n",json_buffer);                             \
            char * error_string;                                    \
            if(asprintf(&error_string,                              \
                        __VA_ARGS__))                               \
            {                                                       \
                _test_failed("JSON buffer is incorrect at \"%s\".", \
                             error_string);                         \
            }                                                       \
        }                                                           \
        free(json_buffer);                                          \
    }

#define _should_be(JSON,...)                    \
    _cdict_should_be(cdict,(JSON),__VA_ARGS__);

static int _cdict_test_API_macros(void)
{
    /*
     * Test the many API macros that wrap the C functions.
     *
     * We do this by comparing to strings that hold JSON buffers.
     */

    CDict_New(cdict);

    /*
     * make a new cdict and delete it
     */
    {
        _should_be("{}",
                   "Fresh cdict");

        if(cdict->ancestor != cdict)
        {
            _test_failed("Fresh cdict's ancestor should be itself.");
        }

        cdict->use_cache = TRUE;
        CDict_New(child,cdict);
        _should_be("{}",
                   "cdict after child added");
        if(child->parent != cdict)
        {
            _test_failed("Child's parent is incorrect.");
        }
        if(child->ancestor != cdict->ancestor)
        {
            _test_failed("Child's ancestor is incorrect.");
        }
        if(child->use_cache != cdict->use_cache)
        {
            _test_failed("Child should inherit use_cache.");
        }
        CDict_free(child); /* no longer required */
    }

    /*
     * Check the set, del and contains macros,
     * also cdict's asprintf
     */
    {

        CDict_set(cdict,"key 1","value 1");
        _should_be("{\"key 1\":\"value 1\"}",
                   "Set test 1");

        CDict_set(cdict,"key 2","value 2 with metadata","some metadata");
        _should_be("{\"key 1\":\"value 1\",\"key 2\":\"value 2 with metadata\"}",
                   "Set test 2");

        char * metadata_string;
        if(asprintf(&metadata_string,"some metadata")==0)
        {
            _test_failed("asprintf failed to alloc");
        }
        CDict_set(cdict,
                  "key 3",
                  "value 3 with metadata and free function",
                  (void*)metadata_string,
                  _metadata_freer);
        CDict_Safe_free(cdict,
                        metadata_string);
        _should_be("{\"key 1\":\"value 1\",\"key 2\":\"value 2 with metadata\",\"key 3\":\"value 3 with metadata and free function\"}",
                   "Set test 3");

        CDict_set_with_types(cdict,
                             "key 4",CDICT_DATA_TYPE_STRING,
                             "value 4",CDICT_DATA_TYPE_STRING);
        _should_be("{\"key 1\":\"value 1\",\"key 2\":\"value 2 with metadata\",\"key 3\":\"value 3 with metadata and free function\",\"key 4\":\"value 4\"}",
                   "Set test 4");

        CDict_set_with_types_and_formats(cdict,
                                         "key 5",CDICT_DATA_TYPE_STRING,"%30s",
                                         (float)1.0,CDICT_DATA_TYPE_FLOAT,"%e");

        _should_be("{\"key 1\":\"value 1\",\"key 2\":\"value 2 with metadata\",\"key 3\":\"value 3 with metadata and free function\",\"key 4\":\"value 4\",\"                         key 5\":1}",
                   "Set test 5");

        char * more_metadata_string;
        if(asprintf(&more_metadata_string,"some more metadata") == 0)
        {
            _test_failed("asprintf failed to alloc");
        }
        CDict_set_with_types_formats_and_metadata(cdict,
                                                  "key 6",CDICT_DATA_TYPE_STRING,"%30s",
                                                  (float)1.0,CDICT_DATA_TYPE_FLOAT,"%e",
                                                  (void*)more_metadata_string,
                                                  _metadata_freer);
        _should_be("{\"key 1\":\"value 1\",\"key 2\":\"value 2 with metadata\",\"key 3\":\"value 3 with metadata and free function\",\"key 4\":\"value 4\",\"                         key 5\":1,\"                         key 6\":1}",
                   "Set test 6");

        /* remove key 3 which also has metadata : also tests CDict_contains() */
        CDict_del_and_contents(cdict,CDict_contains(cdict,"key 3"),FALSE);
        _should_be("{\"key 1\":\"value 1\",\"key 2\":\"value 2 with metadata\",\"key 4\":\"value 4\",\"                         key 5\":1,\"                         key 6\":1}",
                   "Del test 1");

        /* remove a key that doesn't exist */
        CDict_del(cdict,CDict_contains(cdict,"key that does not exist"));

        _should_be("{\"key 1\":\"value 1\",\"key 2\":\"value 2 with metadata\",\"key 4\":\"value 4\",\"                         key 5\":1,\"                         key 6\":1}",
                   "Del test 2");

        char * string;
        struct cdict_entry_t * entry = CDict_contains(cdict,"key 2");
        CDict_entry_to_key_string(cdict,entry,">>> %s <<<");
        if(strcmp(entry->key.string,
                  ">>> key 2 <<<")!=0)
        {
            _test_failed("CDict_entry_to_key_string did not give \">>> key 2 <<<\" as required. %s",entry->key.string);
        }
        CDict_entry_to_value_string(cdict,entry,string,">>> %s <<<");
        if(strcmp(string,
                  ">>> value 2 with metadata <<<")!=0)
        {
            _test_failed("CDict_entry_to_value_string did not give \">>> value 2 with metadata <<<\" as required.");
        }
        CDict_Safe_free(cdict,string);

        CDict_entry_to_key_and_value_strings(cdict, entry, string);
        if(strcmp(entry->key.string,
                  "key 2")!=0)
        {
            _test_failed("CDict_entry_to_key_and_string_values did not give key \"key 2\" as required. %s",entry->key.string);
        }
        if(strcmp(string,
                  "value 2 with metadata")!=0)
        {
            _test_failed("CDict_entry_to_value_string did not give value \"value 2 with metadata\" as required. %s",string);
        }
        CDict_Safe_free(cdict,string);
    }

    /*
     * Test copy and free macros
     */
    {
        CDict_New(copy);
        CDict_copy(cdict,copy);

        _cdict_should_be(copy,"{\"key 1\":\"value 1\",\"key 2\":\"value 2 with metadata\",\"key 4\":\"value 4\",\"key 5\":1,\"key 6\":1}", "cdict copy is incorrect");

        CDict_free(copy);
        CDict_renew(copy);

        /*
         * Make a string using cdict's asprintf
         */
        char * s_libcdict;
        CDict_asprintf(copy,s_libcdict,"value test made by cdict");
        CDict_nest(copy,
                   "made by cdict",s_libcdict,
                   "nested dict");

        /*
         * Make a string using libbsd's asprintf
         */
        char * s_libbsd;
        if(asprintf(&s_libbsd,"value test made by libbsd")==0)
        {
            _test_failed("Failed to allocate memory with asprintf");
        }
        CDict_nest(copy,
                   "made by libbsd",(char*)s_libbsd,
                   "nested dict");

        struct cdict_entry_t * const ee =
            CDict_nest_get_entry(copy,
                                 "nested dict","made by libbsd");
        ee->value.type = CDICT_DATA_TYPE_CHAR_POINTER;
        _cdict_should_be(copy,
                         "{\"nested dict\":{\"made by cdict\":\"value test made by cdict\",\"made by libbsd\":\"value test made by libbsd\"}}",
                         "copy with multiple strings is incorrect");
        CDict_free_and_free_contents(copy);
        if(copy!=NULL)
        {
            _test_failed("after test of CDict_free_and_free_contents() the dict copy is not freed and NULL");
        }
    }


    {
        /*
         * Repeat the above test with a custom function to
         * free metadata.
         */
        CDict_New(copy);

        char * s_libcdict;
        CDict_asprintf(copy,s_libcdict,"value test made by cdict");
        CDict_nest(copy,
                   "made by cdict",s_libcdict,
                   "nested dict");

        /*
         * Make a string using libbsd's asprintf
         */
        char * s_libbsd;
        if(asprintf(&s_libbsd,"value test made by libbsd")==0)
        {
            _test_failed("Failed to allocate memory with asprintf");
        };
        CDict_nest(copy,
                   "made by libbsd",(char*)s_libbsd,
                   "nested dict");

        struct cdict_entry_t * const ee =
            CDict_nest_get_entry(copy,
                                 "nested dict","made by libbsd");
        ee->value.type = CDICT_DATA_TYPE_CHAR_POINTER;
        _cdict_should_be(copy,
                         "{\"nested dict\":{\"made by cdict\":\"value test made by cdict\",\"made by libbsd\":\"value test made by libbsd\"}}",
                         "copy with multiple strings is incorrect");
        CDict_free_with_metadata_function(copy,
                                          _metadata_freer);
        free(s_libbsd);

        if(copy!=NULL)
        {
            _test_failed("after test of CDict_free_with_metadata_function() the dict copy is not freed and NULL");
        }

        CDict_free(cdict);

        if(cdict!=NULL)
        {
            _test_failed("cdict should be NULL after being freed");
        }
        if(copy!=NULL)
        {
            _test_failed("copy should be NULL after being freed");
        }
    }

    CDict_renew(cdict);
    _should_be("{}",
               "Fresh renewed cdict");

    CDict_free(cdict);

    /*
     * test append macros
     */
    {
        struct cdict_entry_t * ee;
        CDict_renew(cdict);
        ee = CDict_set(cdict,
                       "integer",(int)1);
        _cdict_should_be(cdict,"{\"integer\":1}","failed to set \"integer\" key to value 1.");
        if(ee==NULL)
        {
            _test_failed("Set of key=integer, value=1 failed to return an entry.");
        }
        ee = CDict_append(cdict,
                          "integer",(int)1);
        _cdict_should_be(cdict,"{\"integer\":2}","failed to append 1 to integer value of 1 to make 2.");
        if(ee==NULL)
        {
            _test_failed("Append of 1 to 1 failed to return an entry.");
        }

        ee = CDict_append_with_types(cdict,
                                     "integer",CDICT_DATA_TYPE_STRING,
                                     4,CDICT_DATA_TYPE_INT);
        _cdict_should_be(cdict,"{\"integer\":6}","failed to append (with types) 4 to integer value of 2 to make 6.");
        if(ee==NULL)
        {
            _test_failed("Append of 4 to 2 failed to return an entry.");
        }
    }

    /*
     * test contains macros
     */
    {
        struct cdict_entry_t * ee = CDict_contains_with_type(cdict,"integer",CDICT_DATA_TYPE_STRING);
        if(ee==NULL)
        {
            _test_failed("Failed to find key \"integer\" with CDict_contains_with_type in cdict which contains only one key \"integer\".");
        }
        ee = CDict_contains(cdict,"integer");
        if(ee==NULL)
        {
            _test_failed("Failed to find key \"integer\" with CDict_contains in cdict which contains only one key \"integer\".");
        }
        ee = CDict_contains_with_type(cdict,"not a real key",CDICT_DATA_TYPE_STRING);
        if(ee != NULL)
        {
            _test_failed("Found \"not a real key\" (type string) in the cdict when it does not exist");
        }
        ee = CDict_contains_with_type(cdict,"not a real key",CDICT_DATA_TYPE_INT);
        if(ee != NULL)
        {
            _test_failed("Found \"not a real key\" (type int) in the cdict when it does not exist");
        }
        ee = CDict_contains(cdict,"not a real key");
        if(ee != NULL)
        {
            _test_failed("Found \"not a real key\" (no type specified) in the cdict when it does not exist");
        }
    }

    /*
     * Test converting data to strings
     */
    {
        struct cdict_entry_t * ee = CDict_contains(cdict,"integer");
        if(ee == NULL)
        {
            _test_failed("Failed to find ee in cdict, this should have failed previously.");
        }

        char * _string;
        CDict_entry_to_value_string(cdict,
                                    ee,
                                    _string,
                                    "%d");
        if(strcmp(_string,"6")!=0)
        {
            _test_failed("entry should have converted to string \"6\"");
        }
        free(_string);

        CDict_entry_to_value_string(cdict,
                                    ee,
                                    _string,
                                    NULL);
        if(strcmp(_string,"6")!=0)
        {
            _test_failed("entry should have converted value to string \"6\"");
        }
        free(_string);

        CDict_entry_to_key_string(cdict,
                                  ee,
                                  NULL);
        if(strcmp(ee->key.string,"integer")!=0)
        {
            _test_failed("entry should have converted key to string \"integer\"");

        }

        struct cdict_key_t * const key = &ee->key;
        struct cdict_value_t * const value = &ee->value;
        CDict_key_and_value_strings(cdict,key,value,_string);
        if(strcmp(_string,"6")!=0)
        {
            _test_failed("entry should have converted value to string \"6\"");
        }
        if(strcmp(ee->key.string,"integer")!=0)
        {
            _test_failed("entry should have converted key to string \"integer\"");
        }
        free(_string);

        /*
         * Check we can get data from an entry
         */
        double double_array[] = {1.0,2.0,3.0};
        CDict_set(cdict,
                  "double array",double_array);
        ee = CDict_contains(cdict,"double array");
        CDict_entry_index_to_value_string(cdict, ee, _string, NULL, 0);
        if(strcmp(_string,"1.0")!=0)
        {
            _test_failed("double array element 0 should give \"1.0\", it is actually \"%s\".",_string);
        }
        free(_string);
        CDict_entry_index_to_value_string(cdict, ee, _string, NULL, 1);
        if(strcmp(_string,"2.0")!=0)
        {
            _test_failed("double array element 1 should give string \"2.0\".");
        }
        free(_string);
        CDict_entry_index_to_value_string(cdict, ee, _string, NULL, 2);
        if(strcmp(_string,"3.0")!=0)
        {
            _test_failed("double array element 2 should give string \"3.0\".");
        }
        free(_string);

        /*
         * Repeat but access data from the value union
         */
        CDict_value_index_to_value_string(cdict, &ee->value, _string, NULL, 0);
        if(strcmp(_string,"1.0")!=0)
        {
            _test_failed("double array element 0 should give \"1.0\" but is \"%s\".",_string);
        }
        free(_string);
        CDict_value_index_to_value_string(cdict, &ee->value, _string, NULL, 1);
        if(strcmp(_string,"2.0")!=0)
        {
            _test_failed("double array element 1 should give string \"2.0\" but is \"%s\".",_string);
        }
        free(_string);
        CDict_value_index_to_value_string(cdict, &ee->value, _string, NULL, 2);
        if(strcmp(_string,"3.0")!=0)
        {
            _test_failed("double array element 2 should give string \"3.0\" but is \"%s\".",_string);
        }
        free(_string);
    }
    CDict_free(cdict);

    /*
     * Test sort function
     */
    {
        CDict_renew(cdict);
        CDict_nest(cdict,
                   "key 2","1",
                   "nest location");
        CDict_nest(cdict,
                   "key 4","4",
                   "nest location");
        CDict_nest(cdict,
                   "key 3","2",
                   "nest location");
        CDict_nest(cdict,
                   "key 1","3",
                   "nest location");
        struct cdict_entry_t * const ee = CDict_contains(cdict,"nest location");
        struct cdict_t * const nested_cdict = ee->value.value.cdict_pointer_data;

        /* auto sort keys */
        CDict_sort(nested_cdict);
        _cdict_should_be(cdict,
                         "{\"nest location\":{\"key 1\":\"3\",\"key 2\":\"1\",\"key 3\":\"2\",\"key 4\":\"4\"}}",
                         "cdict is not sorted correctly (auto sort).");

        /* reverse sort keys */
        CDict_reverse_sort(nested_cdict);
        _cdict_should_be(cdict,
                         "{\"nest location\":{\"key 4\":\"4\",\"key 3\":\"2\",\"key 2\":\"1\",\"key 1\":\"3\"}}",
                         "cdict is not sorted correctly (reverse sort).");

        /* sort values by function */
        CDict_sort_by_func(nested_cdict,cdict_sort_by_entry_test);
        _cdict_should_be(cdict,
                         "{\"nest location\":{\"key 2\":\"1\",\"key 3\":\"2\",\"key 1\":\"3\",\"key 4\":\"4\"}}",
                         "cdict is not sorted correctly (sort by func).");
        CDict_free(cdict);
    }

    /*
     * Test sorted loop
     */
    {
        CDict_renew(cdict);
        CDict_nest(cdict,
                   "key 2","1",
                   "nest location");
        CDict_nest(cdict,
                   "key 4","4",
                   "nest location");
        CDict_nest(cdict,
                   "key 3","2",
                   "nest location");
        CDict_nest(cdict,
                   "key 1","3",
                   "nest location");

        int int_array[4];
        int i = 0;
        struct cdict_t * const nested_cdict =
            CDict_contains(cdict,
                           "nest location")->value.value.cdict_pointer_data;

        int int_array_should_be[] = {3,1,2,4};
        CDict_sorted_loop(nested_cdict,_entry)
        {
            int_array[i++] = atoi(_entry->value.value.string_data);
        }

        for(i=0;i<4;i++)
        {
            if(int_array[i] != int_array_should_be[i])
            {
                _test_failed("sorted loop test : int array element %d is %d when it should be %d",
                             i,
                             int_array[i],
                             int_array_should_be[i]);
            }
        }

        /*
         * Test reverse sort
         */
        CDict_reverse_sort(nested_cdict);

        i=0;
        CDict_reverse_sorted_loop(nested_cdict,_entry)
        {
            int_array[i] = atoi(_entry->value.value.string_data);
        }

        for(i=0;i<4;i++)
        {
            if(int_array[i] != int_array_should_be[i])
            {
                _test_failed("sorted loop test : int array element %d is %d when it should be %d",
                             i,
                             int_array[i],
                             int_array_should_be[3-i]);
            }
        }
    }
    CDict_free(cdict);
    CDict_renew(cdict);

    {
        /*
         * Test nest functions
         */
        CDict_nest(cdict,"key",1,"location");
        _should_be("{\"location\":{\"key\":1}}", "CDict_nest failed");
        CDict_nest(cdict,"key",1,"location");
        _should_be("{\"location\":{\"key\":2}}", "CDict_nest failed");
        CDict_nest_set(cdict,"key",8.000,"location");
        _should_be("{\"location\":{\"key\":8.0}}", "CDict_nest failed");
        CDict_nest_with_metadata(cdict,"key",8.000,"here is some metadata",NULL,"location");
        _should_be("{\"location\":{\"key\":1.6e1}}", "CDict_nest failed");
        CDict_nest_with_metadata_set(cdict,"key",4.000,"here is some metadata",NULL,"location");
        _should_be("{\"location\":{\"key\":4.0}}", "CDict_nest failed");
        CDict_nest_with_metadata(cdict,"key",2.000,"here is some metadata",NULL,"location");
        _should_be("{\"location\":{\"key\":6.0}}", "CDict_nest failed");
        struct cdict_entry_t * eee = CDict_nest_get_entry(cdict,"location");
        if(eee == NULL)
        {
            _test_failed("failed to find \"location\" nest entry in CDict_nest_get_entry test");
        }
        eee = CDict_nest_get_entry(cdict,"location","key");
        if(eee == NULL)
        {
            _test_failed("failed to find \"location\"->\"key\" nest entry in CDict_nest_get_entry test");
        }
        void * data = CDict_nest_get_data_pointer(cdict,"location","key");
        if(data == NULL)
        {
            _test_failed("failed to get data pointer at \"location\"->\"key\" nest entry in CDict_nest_get_data_pointer test");
        }
        double dbl = 0;
        dbl = CDict_nest_get_data_value(cdict,dbl,"location","key");
        if(dbl != 6.0)
        {
            _test_failed("failed to get data value (should be ) at \"location\"->\"key\" nest entry in CDict_nest_get_data_value test");
        }
    }

    /*
     * Test cdict's asprintf
     */
    {
        CDict_free(cdict);
        CDict_renew(cdict);
        char * hello = CDict_string(cdict,"hello");
        if(strcmp(hello,"hello")!=0)
        {
            _test_failed("CDict_string did not make a string \"hello\"\n");
        }

        char * hello_world;
        CDict_asprintf(cdict,hello_world,"hello %s","world");
        if(strcmp(hello_world,"hello world")!=0)
        {
            _test_failed("CDict_string did not make a string \"hello world\", instead it made %s\n",hello_world);
        }
    }

    /*
     * Test a call of CDict_free_to_free
     */
    {
        CDict_free_to_free_list(cdict);
        CDict_free(cdict);
    }

    /*
     * Test assert
     */
    {
        CDict_renew(cdict);
        int assert_counter = 0;
        cdict->error_handler = _test_assert_error_handler;
        cdict->error_data = &assert_counter;
        CDict_assert(cdict,FALSE);
        if(assert_counter != 1)
        {
            _test_failed("Assert counter should be 1 after FALSE assert");
        }
    }

    /*
     * Test stats
     */
    {
        char * stats;
        {
            const char * const should_be = "Cdict 0x000000000000 (ancestor 0x000000000000, stats (nil)) : nkeys 0, nvalues 0, max_depth 0";
            const char * const could_be = "Cdict 0x0000000 (ancestor 0x0000000, stats (nil)) : nkeys 0, nvalues 0, max_depth 0";
            cdict_stats(cdict,NULL,&stats);
            _trim_trailing_whitespace(stats);
            _remove_pointers(stats);

            if(!(strcmp(stats,should_be)==0 || strcmp(stats,could_be)==0))
            {
                printf("%s\n",stats);
                printf("%s\n",should_be);
                _test_failed("cdict_stats on empty cdict failed \"%s\"",stats);
            }
            free(stats);
        }

        {
            const char * const should_be = "Cdict 0x000000000000 (ancestor 0x000000000000, stats (nil)) : nkeys 5, nvalues 5, max_depth 4";
            const char * const could_be = "Cdict 0x0000000 (ancestor 0x0000000, stats (nil)) : nkeys 5, nvalues 5, max_depth 4";
            double testd = 333.0;
            CDict_nest(cdict,
                       666,"Number of iron maiden",
                       "location",
                       1,
                       666.0,
                       &testd);
            cdict_stats(cdict,NULL,&stats);
            _trim_trailing_whitespace(stats);
            _remove_pointers(stats);
            if(!(strcmp(stats,should_be)==0 || strcmp(stats,could_be)==0))
            {
                _test_failed("cdict_stats on empty cdict failed %s",stats);
            }
            free(stats);
        }
    }

    CDict_free(cdict);

    {
        CDict_renew(cdict);
        CDict_activate_memory_monitoring(cdict);
        CDict_nest(cdict,
                   666,"Number of iron maiden",
                   "location",
                   1,
                   666.0);
        cdict->cdict_entry_list->value.type = 0123;
        cdict->cdict_entry_list->value.value.Boolean_array_data = NULL;
        CDict_free(cdict);
    }

    return CDICT_TEST_PASSED;
}

static void _trim_trailing_whitespace(char * const c)
{
    /*
     * trim trailing whitespace from a string
     */
    char * p = c + strlen(c) -1;
    while(p > c && (*p == ' ' || *p == '\n'))
    {
        *p = '\0';
        p--;
    }
}

static void _remove_pointers(char * const c)
{
    /*
     * Remove pointers, e.g. 0x12345678, from a string
     * replacing them with 0x00000000.
     *
     * This assumes the pointer has a non-digit following it,
     * e.g. a space.
     */
    char * p = c;
    while((p = strstr(p,"0x")) && p)
    {
        //char * const q = p; // location of 0x
        p+=2; // skip 0x

        while(isxdigit(*p))
        // while we match a hex char...
        {
            *p='0';
            p++;
        }
        //        const size_t l = strlen(p);
        //      memmove(q,p,l);
        //*(q+l)='\0';
     }
}

static int _test_assert_error_handler(void * p,
                                      const int error_number __CDict_maybe_unused,
                                      const char * const format __CDict_maybe_unused,
                                      va_list args __CDict_maybe_unused)
{
    /*
     * Test assert error catching: just increment the counter
     */
    int * assert_counter_pointer = p;
    (*assert_counter_pointer)++;
    return -1; /* return -1 : silent error testing */
}


static char * _slurp(char * const file)
{
    /*
     * slurp the contents of file to buffer and return it
     */
    FILE * fp = fopen(file, "r");
    if(fp == NULL) return NULL;
    if(fseek(fp, 0L, SEEK_END) != 0) return NULL;
    const long n = ftell(fp);
    if(fseek(fp, 0L, SEEK_SET)!=0) return NULL;
    char * const buffer = (char*)calloc(n, sizeof(char));
    if(buffer == NULL) return NULL;
    /*
     * if fread fails, just close and return nothing
     * which will cause the test to fail anyway
     */
    if(fread(buffer, sizeof(char), n, fp)){};
    fclose(fp);
    return buffer;
}


#ifdef __HAVE_VALGRIND__
#include "valgrind/valgrind.h"
#endif//__HAVE_VALGRIND__

static Boolean test_for_valgrind(void)
{
    /*
     * Test for whether we are running undef Valgrind.
     *
     * If we are, and we haven't built binary_c using
     * "meson -Dvalgrind=true" (or similar) stop running
     * because we are likely to crash.
     */
    Boolean running_under_valgrind = FALSE;

#ifdef __HAVE_VALGRIND__
    /*
     * If we have valgrind development files installed,
     * use the RUNNING_ON_VALGRIND macro to do the detection.
     */
    if(RUNNING_ON_VALGRIND)
    {
        running_under_valgrind = TRUE;
    }
    else
#endif//__HAVE_VALGRIND__
    {
        /*
         * Fallback: test LD_PRELOAD
         */
        char * const ld_preload = getenv("LD_PRELOAD");
        if(ld_preload &&
           (
               strstr(ld_preload, "/valgrind/") != NULL
               ||
               strstr(ld_preload, "/vgpreload") != NULL)
            )
        {
            running_under_valgrind = TRUE;
        }
    }
#ifndef VALGRIND
    if(running_under_valgrind == TRUE)
    {
        printf("You are trying to run libcdict's test program through valgrind without building with valgrind support. On some architectures this will fail, so please configure libcdict with something like\n\nmeson setup -Dvalgrind=true builddir\n\nand rebuild.\n");
        exit(1);
    }
#endif//VALGRIND

    return running_under_valgrind;
}

static int cdict_sort_by_entry_test(struct cdict_t * const cdict __CDict_maybe_unused,
                                    struct cdict_entry_t * const a,
                                    struct cdict_entry_t * const b)
{
    return strcmp(a->value.value.string_data,
                  b->value.value.string_data);
}


static int _cdict_test_iter(void)
{
    CDict_New(cdict);
    CDict_nest_set(cdict,
                   "array demo",((int[]){1,2,3}),
                   "demo");
    CDict_nest(cdict,
               1,"Bergkamp Arsenal",
               "Denis","10");
    CDict_nest(cdict,
               2,"Henry Arsenal",
               "Denis","10");

    CDict_nest(cdict,
               2.0,"nested data",
               "hello");
    CDict_nest(cdict,
               TRUE,"Bergkamp Ajax",
               "Denis",20);
    CDict_nest_set(cdict,
                   "Robert Pires",7,
                   "Best player ever");

//    printf("JSON:\n\n%s\n",buffer);

    char * json_buffer = NULL;
    size_t json_size = 0;
    CDict_to_JSON(cdict,
                  json_buffer,
                  json_size,
                  CDICT_JSON_WHITESPACE_NO_SORT);
    printf("%s\n",json_buffer);

    struct cdict_iterator_t * iterator = NULL;
    const Boolean sort = TRUE;
    while(TRUE)
    {
        struct cdict_entry_t * const entry = cdict_iter(cdict,
                                                        &iterator,
                                                        sort);

        for(size_t i=0; i<iterator->stack->nstack; i++)
        {
            char * valuestring;
            struct cdict_entry_t * this = iterator->stack->items[i] ? iterator->stack->items[i]->entry : NULL;
            cdict_entry_to_key_and_value_strings(cdict, this, &valuestring);
            printf("\"%s\"",
                   this->key.string);
            if(!CDict_entry_is_hash(this))
            {
                printf(" = %s\n",
                       valuestring);
            }
            else
            {
                printf(", ");
            }
            free(valuestring);
        }

        if(entry == NULL) break;
    }
    cdict_iter(NULL,&iterator,FALSE); // free memory
    CDict_Safe_free(cdict,json_buffer);
    CDict_free(cdict);

    return CDICT_TEST_PASSED;
}


static int __CDict_maybe_unused _cdict_test_JSON_load(void)
{
    /*
     * test loading of JSON : work in progress!
     */
    CDict_new(c);
    CDict_JSON_file_to_CDict(c,"/tmp/JSON_commands");
    CDict_free(c);
    return CDICT_TEST_PASSED;
}

static int _cdict_test_cdict_arrays(void)
{
    CDict_new(a);
    CDict_new(b);
    CDict_new(c);
    CDict_set(c,
              "cdict c",
              "this is c");
    CDict_set(b,
              "cdict b",
              "this is b");

    struct cdict_t * cdict_array[] = {b,c};

    CDict_set(a,
              "cdict array a",
              cdict_array);

    printf("a=%p b=%p c=%p\n",
           (void*)a,(void*)b,(void*)c);

    printf("is cdict_array detected as an array? %d (size %lld) \n",
           CDict_is_array(cdict_array),
           CDict_array_size(cdict_array));

    printf("value type %d %s ... datatype is array? %d \n",
           __CDict_valuetype_num(cdict_array),
           __CDict_typename(cdict_array),
           __CDict_datatype_is_array(__CDict_valuetype_num(cdict_array)));

    CDict_print_JSON(a);
    CDict_print_JSON(b);
    CDict_print_JSON(c);

    CDict_free(a);
    CDict_free(b);
    CDict_free(c);

    return CDICT_TEST_PASSED;
}
