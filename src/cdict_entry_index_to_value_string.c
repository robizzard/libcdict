#define _GNU_SOURCE
#include <stdio.h>
#include "cdict.h"

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_entry_index_to_value_string(struct cdict_t * const cdict,
                                      const struct cdict_entry_t * const entry,
                                      char ** const value_string,
                                      const char * const format,
                                      const cdict_size_t index)
{
    /*
     * Like cdict_value_to_string but for an array, where index
     * is the index of the array value wanted.
     *
     * Only to be used on array types.
     */
    return cdict_value_to_value_string(cdict,
                                       &entry->value,
                                       value_string,
                                       format,
                                       index);
}
