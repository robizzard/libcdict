#include "cdict.h"

/*
 * Given a libcdict type, n, return the appropriate format string,
 * or NULL if none is available.
 */

#undef X
#define X(						\
	  TYPE,						\
	  MEMBER,					\
	  CTYPE,					\
	  DEFCTYPE,					\
	  CARRAY,					\
	  DESCRIPTOR,					\
	  FORMAT,					\
	  DEREF_FORMAT,					\
	  DEREF_CTYPE,					\
	  GROUP,					\
	  ARRAY_TO_SCALAR_MAPPER,			\
	  SCALAR_TO_ARRAY_MAPPER,			\
	  NUMERIC,					\
	  DEMO						\
						)	\
  FORMAT,


__CDict_inline_function char * __CDict_generic_format_string(const CDict_data_type n)
{
    static const char * const __format_strings[] = {
      __CDICT_DATA_TYPES__
    };
    return n < CDICT_DATA_NUMBER_OF_TYPES ? (char *) __format_strings[n] : NULL;
}
#undef X
