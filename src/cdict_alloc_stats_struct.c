#define _GNU_SOURCE
#include <stdio.h>
#include "cdict.h"

__CDict_Nonnull_some_arguments(1)
CDict_API_function
void cdict_alloc_stats_struct(struct cdict_t * const cdict)
{
    /*
     * Allocate memory for the cdict->stats structure
     */
    cdict->stats = __CDict_malloc(cdict,
                                  sizeof(struct cdict_stats_t));
}
