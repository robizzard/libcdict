#include "cdict.h"

/*
 * Get metadata from an entry
 */

__CDict_Nonnull_some_arguments(1)

cdict_metadata_free_f cdict_get_metadata_function(const struct cdict_entry_t * const entry)
{
    return (cdict_metadata_free_f) entry->metadata->free_function;
}
