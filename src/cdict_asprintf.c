#define _GNU_SOURCE
#include <stdio.h>

#include "cdict.h"
#include <stdarg.h>

CDict_API_function
int cdict_asprintf(struct cdict_t * const cdict,
                   char ** const strp,
                   const char * fmt,
                   ...)
{
    /*
     * Wrapper for asprintf to include cdict alloc statistics
     * if required
     */
    va_list vp,vpc;
    va_start(vp,fmt);
    va_copy(vpc,vp);
    const int ret = vasprintf(strp,fmt,vpc);
    if(cdict != NULL &&
       strp != NULL &&
       cdict->ancestor != NULL &&
       cdict->ancestor->stats != NULL)
    {
        cdict->ancestor->stats->asprintf_count++;
        cdict->ancestor->stats->asprintf_size += (size_t)ret;
        cdict_push_pointer(cdict,*strp,(size_t)ret,CDICT_MODE_ASPRINTF);
    }
    va_end(vpc);
    va_end(vp);
    return ret;
}
