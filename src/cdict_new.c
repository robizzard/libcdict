#include "cdict.h"

__CDict_malloc_like
CDict_API_function
struct cdict_t * cdict_new(void)
{
    /*
     * Return a pointer to a new cdict, or NULL on
     * Malloc failure.
     */
    struct cdict_t * c = __CDict_malloc(NULL,sizeof(struct cdict_t));
    cdict_clean_cdict(c);
    return c;
}
