#include "cdict.h"

/*
 * Return the numeric type of variable n.
 */

#undef X
#define X(                                              \
    TYPE,                                               \
    MEMBER,						\
    CTYPE,						\
    DEFCTYPE,                                           \
    CARRAY,                                             \
    DESCRIPTOR,                                         \
    FORMAT,                                             \
    DEREF_FORMAT,                                       \
    DEREF_CTYPE,                                        \
    GROUP,                                              \
    ARRAY_TO_SCALAR_MAPPER,                             \
    SCALAR_TO_ARRAY_MAPPER,                             \
    NUMERIC,                                            \
    DEMO                                                \
    )                                                   \
    CDICT_NUMERIC_##NUMERIC,

CDict_numeric_type __CDict_numeric(const CDict_data_type n)
{
    static const size_t __numerics[] = {
        __CDICT_DATA_TYPES__
    };
    return __numerics[n];
}
#undef X
