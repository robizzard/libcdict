#include "cdict.h"
/*
 * Wrapper to cdict_sort_auto_reverse so we can take entries rather
 * than keys as arguments
 */
__CDict_Pure_function
__CDict_Nonnull_all_arguments
int cdict_sort_by_entry_auto_reverse(struct cdict_t * const cdict,
                                     struct cdict_entry_t * const a,
                                     struct cdict_entry_t * const b)
{
    return cdict_sort_auto_reverse(cdict,
                                   &a->key,
                                   &b->key);
}
