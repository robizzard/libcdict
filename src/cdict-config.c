#include <stdio.h>
#include <stdlib.h>
#include "cdict.h"
#include "ryu/ryu.h"

/*
 * Configuration program for libcdict
 *
 * Usage:
 *
 *     cdict-config <flags>
 *
 * where <flags> are:
 *
 * --prefix : show PREFIX
 * --destdir : show DESTDIR
 * --libs : show linker information
 * --cflags : show compiler flags
 * --include_dir : show the installed include directory
 * --buildflags : show flags used to build libcdict
 * --version : show libcdict version
 * --tests : run a series of tests and show the output in JSON format.
 *           [only if compiled with gcc]
 * --git_url : show git repository URL
 * --git_revision : show git checkout revision
 * --meson_info : show meson build information
 */

static void help(void);

int main (int argc,
          char **  argv)
{
    if(argc>1)
    {
        char *c = *(argv+1);
        while(*c == '-')
        {
            c++;
        }
        if(strncmp(c,"prefix",6)==0)
        {
            printf("%s\n",PREFIX);
        }
        else if(strncmp(c,"destdir",7)==0)
        {
            printf("%s\n",DESTDIR);
        }
        else if(strncmp(c,"libs",4)==0)
        {
            printf("-L%s%s/%s\n",DESTDIR,PREFIX,"lib");
        }
        else if(strncmp(c,"cflags",6)==0)
        {
            printf("-I%s%s/%s\n",DESTDIR,PREFIX,"include");
        }
        else if(strncmp(c,"include_dir",6)==0)
        {
            printf("%s%s/%s\n",DESTDIR,PREFIX,"include/cdict");
        }
        else if(strncmp(c,"buildflags",10)==0)
        {
            printf("%s\n",CFLAGS);
        }
        else if(strncmp(c,"git_revision",12)==0)
        {
#ifdef CDICT_GIT_REVISION
            printf("%s\n",CDICT_GIT_REVISION);
#else
            printf("unknown\n");
#endif
        }
        else if(strncmp(c,"git_url",7)==0)
        {
#ifdef CDICT_GIT_URL
            printf("%s\n",CDICT_GIT_URL);
#else
            printf("unknown\n");
#endif
        }
        else if(strncmp(c,"meson_info",10)==0)
        {
            printf("meson project name %s\n",__CDict_Stringify_macro(MESON_PROJECT_NAME));
            printf("meson project version %s\n",__CDict_Stringify_macro(MESON_PROJECT_VERSION));
        }
        else if(strncmp(c,"version",7)==0)
        {
            printf("libcdict %s : git %s %s\n",
                   CDICT_VERSION,
#ifdef CDICT_GIT_REVISION
                   CDICT_GIT_REVISION,
#else
                   "unknown",
#endif
#ifdef CDICT_GIT_URL
                   CDICT_GIT_URL
#else
                   "unknown"
#endif
                );
        }
        else if(strncmp(c,"test",4)==0)
        {
            cdict_tests();
        }
        else
        {
            help();
        }
    }
    else
    {
        help();
    }
    exit(0);
}

static void help(void)
{
       printf("Usage:\n\ncdict-config <flags>\n\nwhere <flags> are:\n\n--prefix : show PREFIX\n--destdir : show DESTDIR\n--libs : show linker information\n--cflags : show compiler flags\n--include_dir : show the directory where header files are installed--buildflags : show flags used to build libcdict\n--version : show libcdict version\n--tests : run a series of tests and show the output in JSON format.\n--git_url : show git repository URL\n--git_revision : show git checkout revision\n--meson_info : show meson build information\n\n");
}
