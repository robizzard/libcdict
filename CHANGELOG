24/04/2020 : V1.0 initial commits

27/05/2020 : reworked how string versions of keys are cached, this speeds up searching large hashes by x100 or more. Fixed many small bugs, e.g. the right hashing function is now used, C99 is now checked for properly on C11 mode, Booleans are handled properly (TRUE and FALSE are now case as (bool) types), nested hasher works better, fixed some memory leaks.

30/11/2020 : V1.22 updated to include -Wpedantic in the build flags

24/01/2021 : Fixed bug where some strings had the wrong hash keys generated

22/02/2021 : Wrapped function macros to make sure variable names are unique.

28/05/2021 : Fixed bug where Booleans are uses at as keys and misidentified.

19/06/2021 : V1.26 Added a version of fast_strtod to improve speed.

29/04/2022 : V1.27 Rebranded as libcdict, added some build options,
             released under the GPL.

26/07/2022 : V1.29 Many (mostly small) bug fixes, improved much code, new set of unit tests for the API functions (macro and non-macro calls). Added JOSS paper.

04/08/2022 : V1.30  Add FORTRAN interface and example code (many thanks to Daniel Nemergut).

01/10/2022 : V1.31 Added checks for __VA_OPT__ in meson.build because Clang 11 behaves differently to Clang 12+ and GCC, and set the default C std to gnu18.

06/12/2023 : V1.4 Release for (hopefully!) final JOSS paper.

07/12/2023 : V1.41 fix citation typo and added a few recent papers that use libcdict (thanks to David Hendriks).

15/06/2024 : V1.5 added cdict_iter, an iterator for nested cdicts.

07/07/2024 : V1.51 restructed structures to save ~25% memory, other minor fixes/cleaning of code, few extra subroutines to improve allocating memory.

30/07/2024 : V1.52 bug fixes in cdict_json_to_c, updated ryu double formatting to make sure doubles are doubles, e.g. "1.0" not "1" (which would be an int), shift cprint_asprintf to its own file, can now read doubles starting with "+", add entry->pre_output_function, started to make code for cdict array type. Note still some small memory leaks in cdict_json_to(). 
