#!/usr/bin/env perl
use strict;
use rob_misc;

# script to make and well-space the X-macro table in file
# table.dat

my $file = $ARGV[0] // '../src/cdict_data_types.h';

my @table = split(/\n/,slurp($file));
my @spacings;
for my $l (@table)
{
    my $line = $l;
    if($line =~/X\(/)
    {
        $line=~s/^\s*//;
        $line=~s/X\(//;
        $line=~s/\)\s*\\\s*$//;
        my @x = split(/,/,$line);
        for(my $i=0;$i<=$#x;$i++)
        {
            if(!defined($spacings[$i]))
            {
                $spacings[$i] = length($x[$i]) + 1;
            }
            else
            {
                $spacings[$i] = MAX($spacings[$i],1 + length($x[$i]));
            }
        }
    }
}
for my $line (@table)
{
    if($line =~/X\(/)
    {
        $line=~s/^\s*//;
        $line=~s/X\(//;
        $line=~s/\)\s*\\\s*$//;
        my @x = split(/,/,$line);
        my @out;
        for(my $i=0;$i<=$#x;$i++)
        {
            my $n = $spacings[$i];
            my $format = '%'.$n.'s';
            push(@out,
                 sprintf($format,$x[$i]));
        }
        print "    X(",join(',',@out),") \\\n";
    }
    else
    {
        print $line,"\n";
    }
}

exit;
############################################################

sub slurp
{
    open(my $fh,'<',$_[0])||die("cannot open $_[0] in slurp");
    return (do { local( $/ ) ; <$fh> } );
}

sub MAX
{
    # Usage: MAX($x,$y)
    # Returns the maximum of $x and $y, first checking if the values are numeric.
    if(!is_numeric($_[0]))
    {
        return $_[1];
    }
    elsif(!is_numeric($_[1]))
    {
        return $_[0];
    }
    else
    {
        return $_[0] > $_[1] ? $_[0] : $_[1];
    }
}
